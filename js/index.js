const inputPrice = document.createElement('input');
document.body.append(inputPrice)

inputPrice.addEventListener('focus', event => {
    inputPrice.style.border = '2px solid green';
})
inputPrice.addEventListener('blur', event => {
    inputPrice.style.border = '2px solid red';

    if (inputPrice.value < 0  ) {
        inputPrice.style.border = '2px solid red';
        const alertMessage = document.createElement('span')

        alertMessage.innerText = 'Please enter correct price';
        alertMessage.style.display = 'flex'
        document.body.append(alertMessage)
        inputPrice.addEventListener('click', event=>{
            alertMessage.remove()
        })
    } else{
        const spanMessage = document.createElement('span')
        spanMessage.innerText = `Текущая цена: ${inputPrice.value}`
        spanMessage.style.display = 'flex'
        document.body.prepend(spanMessage)
        const spanRemover = document.createElement('span')
        spanRemover.innerText = 'X'
        spanRemover.style.border = '1px solid blue'
        spanMessage.append(spanRemover)
        spanRemover.addEventListener('click', event => {
            spanMessage.remove()
            spanRemover.remove()
            inputPrice.value = ''

        })
    }

})